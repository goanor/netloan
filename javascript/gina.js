/*
 *	The interface exposed to NLGina.dll consists of:
 *		- getGinaHtmlVersion()
 *		- showInfo()
 *		- askForSessionPin()
 *		- enableGuestcodes()
 *	If one of these functione doesn't exist, the gina falls back to the bahavior from version
 *	4.0 and older for thos functions, i.e. fills/show/hides tags instead of calling scripts.
 *
 *	The default html pages call:
 *		- loaded()
 *		- onLogin()
 *		- onCancel()
 *		- handleKeypress()
 */

// getGinaHtmlVersion() should return the string "4.1".
function getGinaHtmlVersion() {
    return "4.1";
}

// Show info is called by the gina periodically to update the status shown to the user.
// A null-parameter means leave unchanged.
function showInfo(timeString, nextUserLogin, nextUserFull, availableUntil, unitName, machineName, message) {
    var elem;

    // Name = 0, LoginName = 1, LastFourCharactersOfLoginName = 2, ThisMachineHasBeenBooked = 3
    var clientUserNameView = external.GetSetting('ClientUserNameView');
    // UnitNameAndMachineName = 0, UnitName = 1, Nothing = 2
    var clientUnitNameView = external.GetSetting('ClientUnitNameView');

    // If the paramter is null or the element doesn't exist then just ignore it.
    if (timeString != null && null != (elem = document.getElementById("CurrentTime"))) {
        elem.innerText = timeString;
    }

    if (null != (elem = document.getElementById("NextUserLogin"))) {
        if (clientUserNameView == 0) {
            if (nextUserFull != null)
                elem.innerText = nextUserFull;
        } else if (clientUserNameView == 1) {
            if (nextUserLogin != null)
                elem.innerText = nextUserLogin;
        } else if (clientUserNameView == 2) {
            if (nextUserLogin != null) {
                if (nextUserLogin.length > 0)
                    elem.innerText = "****" + nextUserLogin.substring(nextUserLogin.length - 4, nextUserLogin.length);
                else
                    elem.innerText = "";
            }
        } else if (clientUserNameView == 3) {
			if (nextUserLogin != null) {
				if (nextUserLogin.length > 0) {
					elem.innerText = external.GetString("GeneralSettingsClientUserMachineHasBooked");
				}
			}
            else
			{
                elem.innerText = "";
			}
        }
    }

    if (nextUserFull != null && null != (elem = document.getElementById("NextUserFull"))) {
        elem.innerText = nextUserFull;
    }

    if (availableUntil != null && null != (elem = document.getElementById("AvailableUntilTime"))) {
        elem.innerText = availableUntil;
    }

    if (availableUntil != null && null != (elem = document.getElementById("AvailableUntil"))) {
        elem.style.display = (availableUntil != "") ? "inline" : "none";
    }

    if (unitName != null && null != (elem = document.getElementById("UnitNameText")) && clientUnitNameView != 2) {
        elem.innerText = unitName;
    }

    if (unitName != null && null != (elem = document.getElementById("UnitInfo"))) {
        elem.style.display = (unitName != "") && (clientUnitNameView != 2) ? "inline" : "none";
    }

    if (machineName != null && null != (elem = document.getElementById("MachineNameText")) && clientUnitNameView == 0) {
        elem.innerText = machineName;
    }

    if (clientUnitNameView != 0 && null != (elem = document.getElementById("MachineNameTextStart"))) {
        elem.style.display = "none";
    }

    if (clientUnitNameView != 0 && null != (elem = document.getElementById("MachineNameTextEnd"))) {
        elem.style.display = "none";
    }

    if (message != null && null != (elem = document.getElementById("Message"))) {
        elem.innerText = message;
        elem.style.display = (message!="")?"block":"none";
    }
}

var _askForSessionPin = false;
var _guestEnableLogin = false;
var _guestEnableCreateNew = false;
var _guestIsSelected = false;

// This is called to show or hide the input field that asks for a session password.
function askForSessionPin(bAsk) {
    _askForSessionPin = bAsk;

    var elem;
    if (null != (elem=document.getElementById("sessionpasswordblock")))
        elem.style.display = bAsk?"block":"none";
}

// If guest codes are used, then enableGuestcodes(true) is called by the gina after
// the html pages has loaded.
function enableGuestcodes(enableLogin, enableCreateNew, isGuestSelected) {
    if (enableLogin == null)
        enableLogin = _guestEnableLogin;
    if (enableCreateNew == null)
        enableCreateNew = _guestEnableCreateNew;
    if (isGuestSelected == null)
        isGuestSelected = _guestIsSelected;

    _guestEnableLogin = enableLogin;
    _guestEnableCreateNew = enableCreateNew;
    _guestIsSelected = isGuestSelected;

    var hasChanged = false;
    var dispMode;
    var elem;
    if (null != (elem=document.getElementById("memberblock"))) {
        dispMode = elem.style.display;
        elem.style.display = (!enableLogin || !isGuestSelected)?"block":"none";
        if (dispMode != elem.style.display) {
            hasChanged = true;
        }
    }
    if (null != (elem=document.getElementById("guestblock"))) {
        dispMode = elem.style.display;
        elem.style.display = (enableLogin && isGuestSelected)?"block":"none";
        if (dispMode != elem.style.display) {
            hasChanged = true;
        }
    }
    if (null != (elem=document.getElementById("GuestInfo"))) {
        dispMode = elem.style.display;
        elem.style.display = (enableLogin && !isGuestSelected)?"block":"none";
        if (dispMode != elem.style.display) {
            hasChanged = true;
        }
    }
    if (null != (elem=document.getElementById("GuestBtn"))) {
        dispMode = elem.style.display;
        elem.style.display = (enableLogin && !isGuestSelected)?"inline":"none";
        if (dispMode != elem.style.display) {
            hasChanged = true;
        }
    }
    if (null != (elem=document.getElementById("MemberInfo"))) {
        dispMode = elem.style.display;
        elem.style.display = (enableLogin && isGuestSelected)?"block":"none";
        if (dispMode != elem.style.display) {
            hasChanged = true;
        }
    }
    if (null != (elem=document.getElementById("MemberBtn"))) {
        dispMode = elem.style.display;
        elem.style.display = (enableLogin && isGuestSelected)?"inline":"none";
        if (dispMode != elem.style.display) {
            hasChanged = true;
        }
    }
    if (null != (elem=document.getElementById("NewGuestBtn"))) {
        dispMode = elem.style.display;
        elem.style.display = (enableLogin && enableCreateNew && isGuestSelected)?"inline":"none";
        if (dispMode != elem.style.display) {
            hasChanged = true;
        }
    }

    if (hasChanged) {
        var visInputs = getVisibleInputs();
        if(visInputs.length > 0) {
            visInputs[0].focus();
        }
    }
}

function loaded() {
    // Set focus to the first visible input.
    if (typeof getVisibleInputs === "function" && getVisibleInputs().length > 0)
        getVisibleInputs()[0].focus();
}

function onLogin() {
    var elem;
    if ((elem=document.getElementsByName("username")).length > 0)
        elem[0].disabled = (_guestEnableLogin && _guestIsSelected);
    if ((elem=document.getElementsByName("password")).length > 0)
        elem[0].disabled = (_guestEnableLogin && _guestIsSelected);
    if ((elem=document.getElementsByName("sessionpassword")).length > 0)
        elem[0].disabled = (_guestEnableLogin && _guestIsSelected || !_askForSessionPin);
    if ((elem=document.getElementsByName("guestcode")).length > 0)
        elem[0].disabled = (!_guestEnableLogin || !_guestIsSelected);

    form.action.value="login";
}

function onNewGuestcode() {
    document.getElementsByName("guestcode")[0].value = "new";
    onLogin();

    form.submit();
}

function onCancel() {
    form.action.value="cancel";     //Action should be "cancel" if login is cancelled
    form.submit();
}

function onLang() {
    if(form.LangId.value != form.selectLanguage.value)
    {
        form.LangId.value = form.selectLanguage.value;
        form.action.value="changeLanguage";
        // Should use "get" when it is a custom gina.
        // form.method = "get";

        form.submit();
    }
}

function handleKeypress() {

    // If this isn't an enter. Then just return.
    if (window.event.keyCode != 13)
        return;

    //If a button is focused, return
    var buttonInputs = getButtons();
    for (i=0; i<buttonInputs.length; i++) {
        if (window.event.srcElement == buttonInputs[i]) {
            window.event.returnValue=true;
            return;
        }
    }

    var textInputs = getVisibleInputs();

    // If we pressed enter in the last visible input, then submit
    if (window.event.srcElement == textInputs[textInputs.length-1]) {
        onLogin();
        form.submit();
    } else {
        // If we pressed enter in a different input, then set focus to the next one.
        for (i=0; i<textInputs.length-1; i++) {
            if (window.event.srcElement == textInputs[i]) {
                textInputs[i+1].focus();
                break;
            }
        }
    }
    // Don't submit the form automatically.
    window.event.returnValue=false;
}


// Returns an array containing all visible text or password input elements in the document.
function getVisibleInputs() {
    var inputs = document.getElementsByTagName("input");
    var textInputs = new Array();
    for (i=0; i<inputs.length; i++) {
        if ((inputs[i].type == "text" || inputs[i].type == "password") && inputs[i].getBoundingClientRect().top != 0){
            textInputs = textInputs.concat(inputs[i]);
        }
    }
    return textInputs;
}

// Returns an array containing all buttons in the document.
function getButtons() {
    var inputs = document.getElementsByTagName("input");
    var buttonInputs = new Array();
    for (i=0; i<inputs.length; i++) {
        if (inputs[i].type == "submit" || inputs[i].type == "button")
            buttonInputs = buttonInputs.concat(inputs[i]);
    }

    return buttonInputs;
}

// add dropshadow to text if client platform is not nt4
function printDropShadow() {
    try {
        // if not nt4, add dropshadow
        var agt = navigator.userAgent.toLowerCase();
        var isnt4 = ((agt.indexOf("windows nt 4")!=-1));
        if (!isnt4) {
            document.write("<style>");
            document.write("  label {");
            document.write("    filter:progid:DXImageTransform.Microsoft.dropshadow(OffX=2, OffY=2, Color=#FF2F435D', Positive='true');");
            document.write("  }");
            document.write("</style>");
        }
    } catch (e) {
        //Catches all possible errors so that no javascript error is shown on page
    }
}

function localizeGina(external, pageType)
{
    if (external) {
        try {
            localizeLables(external, pageType);
            localizeButtons(external, pageType);
            localizeInfo(external, pageType);
            getAvailableLanguages(external, pageType, "combo");
        } catch (e) {
            //Catches all possible errors so that no javascript error is shown on page
        }
    }
}

function displayNetloanLogo(external)
{
    if (external) {
        try {
            var elem;
            var productLogo = external.GetSetting('ProductLogo');
            if (productLogo != null && null != (elem = document.getElementById("logo"))) {
                elem.src = "images/" + productLogo;
            }
        } catch (e) {
            //Catches all possible errors so that no javascript error is shown on page
        }
    }
}

function displayNetloanBg(external)
{
    if (external) {
        try {
            var elem;
            var productBg = external.GetSetting('ProductBg');
            if (productBg != null && null != (elem = document.getElementById("main"))) {
                elem.style.backgroundImage = "url(images/" + productBg + ")";
            }
        } catch (e) {
            //Catches all possible errors so that no javascript error is shown on page
        }
    }
}

function localizeLables(external, pageType)
{
    var elem;
    // Login page
    if(pageType == "login" || pageType == "lock") {
        if (null != (elem = document.getElementById("f-cardLbl"))) {
            elem.innerText = external.GetString("CardNo");
        }
        if (null != (elem = document.getElementById("f-passLbl"))) {
            elem.innerText = external.GetString("Pin");
        }
        if (null != (elem = document.getElementById("f-sessionpassLbl"))) {
            elem.innerText = external.GetString("SessionPwd");
        }
        if (null != (elem = document.getElementById("f-guestcodeLbl"))) {
            elem.innerText = external.GetString("Guestcode");
        }
        if (null != (elem = document.getElementById("AvailableUntilText"))) {
            elem.innerText = external.GetString("AvailableUntil");
        }
        if (null != (elem = document.getElementById("locked"))) {
            elem.innerText = external.GetString("lockedFoot");
        }
        if (null != (elem = document.getElementById("UnitName"))) {
            elem.innerText = external.GetString("Unit");
        }

    }

    // Change password page
    if(pageType == "changepass")	{
        if (null != (elem = document.getElementById("userNameLbl"))) {
            elem.innerText = external.GetString("ChangePasswordUserName");
        }
        if (null != (elem = document.getElementById("f-cardLbl"))) {
            elem.innerText = external.GetString("ChangePasswordPassword");
        }
        if (null != (elem = document.getElementById("f-passLbl"))) {
            elem.innerText = external.GetString("ChangePasswordConfirmPassword");
        }
    }
}

function localizeButtons(external, pageType)
{
    var elem;

    // Login page / Lock page
    if(pageType == "login" || pageType == "lock") {
        if (null != (elem = document.getElementById("f-loginLbl"))) {
            elem.innerText = external.GetString("Login");
        }
        if (null != (elem = document.getElementById("f-cancelLbl"))) {
            elem.innerText = external.GetString("Cancel");
        }
        if (null != (elem = document.getElementById("GuestBtn"))) {
            elem.innerText = external.GetString("Guest");
        }
        if (null != (elem = document.getElementById("MemberBtn"))) {
            elem.innerText = external.GetString("Member");
        }
        if (null != (elem = document.getElementById("NewGuestBtn"))) {
            elem.innerText = external.GetString("NewGuest");
        }
    }

    // Change Password page
    if(pageType == "changepass") {
        if (null != (elem = document.getElementById("f-loginLbl"))) {
            elem.innerText = external.GetString("ChangePasswordOk");
        }
        if (null != (elem = document.getElementById("f-cancelLbl"))) {
            elem.innerText = external.GetString("ChangePasswordCancel");
        }
    }

    if(pageType == "usagepolicy") {
        //Usage Policy
        if (null != (elem = document.getElementById("UsagePolicyContinueRich"))) {
            elem.innerText = external.GetString("UsagePolicyContinueRich");
        }
        if (null != (elem = document.getElementById("UsagePolicyAcceptRich"))) {
            elem.innerText = external.GetString("UsagePolicyAcceptRich");
        }
        if (null != (elem = document.getElementById("UsagePolicyDeclineRich"))) {
            elem.innerText = external.GetString("UsagePolicyDeclineRich");
        }
    }

    if (pageType == "errorLoadPage") {

        if (null != (elem = document.getElementById("f-cancelLbl"))) {
            elem.innerText = external.GetString("Cancel");
        }
    }
}

function localizeInfo(external, pageType)
{
    var elem;
    // Login page/ Lock page
    if(pageType == "login" || pageType == "lock") {
        if (null != (elem = document.getElementById("GuestInfo"))) {
            elem.innerText = external.GetString("GuestInfo");
        }
        if (null != (elem = document.getElementById("MemberInfo"))) {
            elem.innerText = external.GetString("MemberInfo");
        }
    }

    // Change Password page
    if(pageType == "changepass")	{

        if (null != (elem = document.getElementById("expiredInfo"))) {
            elem.innerHTML = external.GetString("ChangePasswordExpiredInfo");
        }
    }

    if(pageType == "usagepolicy")	{
        if (null != (elem = document.getElementById("UsagePolicyHeadInfoRich"))) {
            elem.innerHTML = external.GetString("UsagePolicyHeadInfoRich");
        }

        if (null != (elem = document.getElementById("UsagePolicyInfoRich"))) {
            elem.innerHTML = external.GetString("UsagePolicyInfoRich");
        }

        if (null != (elem = document.getElementById("UsagePolicyMinutesRich"))) {
            elem.innerHTML = external.GetString("UsagePolicyMinutesRich");
        }

        if (null != (elem = document.getElementById("UsagePolicyBodyInfoRich"))) {
            elem.innerHTML = external.GetString("UsagePolicyBodyInfoRich");
        }
    }

    if (pageType == "errorLoadPage") {

        if (null != (elem = document.getElementById("PageLoadError"))) {
            elem.innerHTML = external.GetString("PageLoadError");
        }

        if (null != (elem = document.getElementById("PageLoadErrorTitle"))) {
            elem.innerHTML = external.GetString("PageLoadErrorTitle");
        }
    }
}

function validateUsagePolicy(external)
{
    if (theForm.action[0].checked)
    {
        theForm.action.value = theForm.action[0].value;
    }
    else if (theForm.action[1].checked)
    {
        theForm.action.value = theForm.action[1].value;
    }
    else
    {
        var text;
        text = external.GetString("UsagePolicyWarningRich");
        alert(text);
        window.event.returnValue = false;
    }
}

function getAvailableLanguages(external, pageType, controlType)
{
    var elem;
    if (pageType == "login") {
        if (null != (elem = document.getElementById("languageBlock"))) {
            elem.innerHTML = external.GetLanguages(controlType);
        }
    }
}

// ********************************************************************************
// Start of the code required for showing the broken screen and overriding it.
//
// setBrokenStatus is the important function as this is called from the application
// when it receives a messages that the unit is broken.
// ********************************************************************************

var isBroken = false;
var isBrokenOverridden = false;

function setBrokenStatus(newIsBroken) {
	isBroken = newIsBroken;
	effectsOfBrokenState();
}

function effectsOfBrokenState() {
	updateDisplayOfElementForBrokenStatus("hideWhenBroken", "none", "inline");
	updateDisplayOfElementForBrokenStatus("showWhenBroken", "inline", "none");
}

function updateDisplayOfElementForBrokenStatus(elementId, displayWhenBroken, displayWhenNotBroken) {
	var element = document.getElementById(elementId);
	
	if (null != element)
	{
		if (isBrokenAndNotOverridden())
		{
			element.style.display = displayWhenBroken;
		}
		else
		{	
			element.style.display = displayWhenNotBroken;
		}
	}
}

function overrideBrokenPage() {
	if (isBrokenAndNotOverridden())
	{	
		isBrokenOverridden = true;
		effectsOfBrokenState();
		setTimeout(stopOverrideOfBroken, 30 * 1000);
	}
}

function stopOverrideOfBroken() {
	isBrokenOverridden = false;
	effectsOfBrokenState();
}

function isBrokenAndNotOverridden() {
	return isBroken && !isBrokenOverridden;
}

// *******************************************************************************
// End of the code required for showing the broken screen and overriding it
// *******************************************************************************